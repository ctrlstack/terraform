# About

Create S3 bucket to be used for storing any failed to deliver flow logs.

# Inputs

- `s3_bucket_name` - the name of the S3 bucket
