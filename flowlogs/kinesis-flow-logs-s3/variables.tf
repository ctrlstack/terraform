variable "s3_bucket_name" {
  description="The name of the bucket where failed delivery records will be stored"
  type=string
}
