variable "delivery_stream_name" {
  description="The name of the delivery stream"
  default="ctrlstack-flowlogs-delivery"
  type=string
}

variable "s3_bucket_name" {
  description="The name of the bucket where failed delivery records will be stored"
  type=string
}

variable "client_id" {
  description="Client id for api access to CtrlStack"
  type=string
}

variable "client_secret" {
  description="Client secret for api access to CtrlStack"
  type=string
  sensitive=true
}
