resource "aws_kinesis_firehose_delivery_stream" "main" {
  name        = var.delivery_stream_name
  destination = "http_endpoint"

  s3_configuration {
    role_arn           = aws_iam_role.firehose.arn
    bucket_arn         = data.aws_s3_bucket.bucket.arn
    buffer_size        = 5
    buffer_interval    = 300
    compression_format = "GZIP"
  }

  http_endpoint_configuration {
    url                = "https://awsvpcflowlogs.global.ctrlstack.com/"
    name               = "CtrlStack flow logs"
    access_key         = "${var.client_id}:${var.client_secret}"
    buffering_size     = 5
    buffering_interval = 60
    role_arn           = aws_iam_role.firehose.arn
    s3_backup_mode     = "FailedDataOnly"

    request_configuration {
      // content_encoding = "GZIP"
    }
  }

  tags = {
    "LogDeliveryEnabled" = "true"
  }
}
