# About

This module creates a kinesis firehose delivery stream with http endpoint in order to send vpc flow logs to CtrlStack.

# Preparation

Obtain client id and secret from CtrlStack.

# Module inputs

- `delivery_stream_name` - the name for the kinesis firehose delivery stream, defaults to "ctrlstack-flowlogs-delivery"
- `s3_bucket_name` - the name of the s3 bucket to store any failed payloads, can be created with the kinesis-flow-logs-s3 module
- `client_id` - id portion of the access key
- `client_secret` - secret portion of the access key
