# About

Creates flow logs for the provided VPC. The flow logs use kinesis firehose destination to deliver the flow logs to CtrlStack.

# Inputs

- `kinesis_firehose_name` - the name of the kinesis firehose delivery stream to use
- `vpc_id` - the vpc_id of the VPC for which to enable the flow logs
