data "aws_kinesis_firehose_delivery_stream" "stream" {
    name = var.kinesis_firehose_name
}
