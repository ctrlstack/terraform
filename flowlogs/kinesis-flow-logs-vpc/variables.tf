variable "kinesis_firehose_name" {
  description="The name of the kinesis firehose stream"
  type=string
}

variable "vpc_id" {
  description="The identifier of the vpc to monitor"
  type=string
}
