# Prerequisites

1. Obtain client id and client secret from CtrlStack
2. Store the client secret as secret in the AWS Secrets Manager

```
aws secretsmanager create-secret --name cs_vpcflows_secret --secret-string '<CLIENT SECRET>'
```

3. Edit the main.tf file to match you preferences
