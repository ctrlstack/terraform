module "s3-failed-delivery" {
  source = "../kinesis-flow-logs-s3"
  s3_bucket_name = "<BUCKET NAME>"
}

module "flowlogs-firehose" {
  depends_on = [module.s3-failed-delivery.bucket]
  source = "../kinesis-flow-logs-exporter"
  s3_bucket_name = module.s3-failed-delivery.bucket.bucket
  client_id = "<CLIENT ID>"
  client_secret = data.aws_secretsmanager_secret_version.client_secret.secret_string
}

module "vpcflowlogs" {
  for_each = toset([
    "vpc-id-1",
    "vpc-id-2",
  ])
  depends_on = [module.flowlogs-firehose.kinesis_firehose]
  source = "../kinesis-flow-logs-vpc"
  kinesis_firehose_name = module.flowlogs-firehose.kinesis_firehose.name
  vpc_id = each.key
}
