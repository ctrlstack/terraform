terraform {
  backend "s3" {
    bucket         = <TF_STATE_BUCKET>
    key            = <TF_STATE_KEY>
    region         = <REGION>
    dynamodb_table = <TF_DDB_TABLE>
  }
}
